# HAKKINDA

LibreOffice Son Sürüm Yükleme ve Eski Sürümleri Silme
1) Bu script ile en güncel stabil LibreOffice sürümünü tar.gz uzantılı olarak indirip, kurulumunu otomatik yapabilirsiniz.

# KURULUM:
* 1- Uçbirim/Terminal açın.
* 2- `sudo apt install git` kodunu yaz enter ile kurulumu gerçekleştir.
* 3- `git clone https://gitlab.com/yahyayildirim/libreoffice.git` kodunu yaz enter yapın.
* 4- Ardından `cd libreoffice` komutu ile klasörün içine girin.
* 5- Son olarak `sudo bash libreoffice` komutunu çalıştırın.
* 6- Sorulan sorulara E veya H yazıp enter yaparak devam edin.
